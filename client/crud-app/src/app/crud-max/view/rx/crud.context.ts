import {Observable} from 'rxjs/Observable';
import {MemoizedSelector, Store} from '@ngrx/store';
import {Crud} from '../../core/crud';
import {StoreActionFactory} from '../../rx/action/store-action.factory';
import {CrudStatus} from '../../rx/model/crud.status.enum';

import {Context} from './context';

import {CrudEntity} from '../../rx/model/crud.entity';
import {FieldDefinition} from '../model/field-definition';
import {CrudType} from '../../rx/model/crud.type.enum';

export abstract class CrudContext<T extends CrudEntity> extends Context<T>
                                  implements Crud {

    public data: Observable<Array<T>>;
    public entity: Observable<T>;
    public fields: Observable<Array<Array<FieldDefinition>>>;

    /*
    * get selectors by state field [data, entity, fields]
    * */
    protected getDataSelector<T>(type: string):
                                  MemoizedSelector<any, Array<CrudEntity> | CrudEntity> {

      return this.getSelector(type, 'data').value;

    }

    protected getEntitySelector<T>(type: string):
                                    MemoizedSelector<any, Array<CrudEntity> | CrudEntity> {

      return this.getSelector(type, 'entity').value;

    }

    protected getFieldsSelector<T>(type: string):
                                    MemoizedSelector<any, Array<CrudEntity> | CrudEntity> {

      return this.getSelector(type, 'fields').value;

    }

    /*
    * set selectors for actual state fields
    * */
    protected setDataSelector<T>(type: string): void {
      this.setSelector(type, 'data');
    }

    protected setEntitySelector<T>(type: string): void {
      this.setSelector(type, 'entity');
    }

    protected setFieldsSelector<T>(type: string): void {
      this.setSelector(type, 'fields');
    }

     /*
    * dispatches a pending action to the store
    * */
    protected dispatch<T extends CrudEntity>(type: CrudType, payload: any,
                                   payloadType: string, status?: CrudStatus) {
      /*
       * checks if payload type is Entity | CrudEntity
       * if is not the payload [string] argument is passed
       * TODO then (if is not Entity | CrudEntity) it should check if payload is string
       * TODO if not an error should occur
       * */
      this.store.dispatch(this.factory.
                  getCrudAction(type, status || CrudStatus.pending, payloadType, payload));

    }

    /*
   * dispatches action read to the store
   * */
    public read(args?: any, payloadType?: any) {
      this.dispatch(CrudType.read, args, payloadType);
    }
    /*
    * DISPATCH APPLICATION LAYOUT ACTIONS
    * */

    //TODO

    /*
    * CRUD API IMPLEMENTATION - dispatch CrudActions to the store
    * */
    /*
    * dispatches action create to the store
    * */
    public create<T>(entity: T, payloadType?: any) {
      this.dispatch(CrudType.create, entity, payloadType);
    }

    /*
    * dispatches action update to the store
    * */
    public update<T>(entity: T, payloadType?: any) {
      this.dispatch(CrudType.update, entity, payloadType);
    }

    /*
    * dispatches action dispose to the store
    * */
    public dispose<T>(entity: T, payloadType?: any) {
      this.dispatch(CrudType.dispose, entity, payloadType);
    }

    /*
     * dispatches action select to the store
     * */
    public select<T extends CrudEntity>(entity: T, payloadType?: any) {
      this.store.dispatch(this.factory.getCrudAction(CrudType.select, CrudStatus.success,
        payloadType, entity));
    }


    constructor(store: Store<any>,
                protected factory: StoreActionFactory) {

      super(store);
    }
}
