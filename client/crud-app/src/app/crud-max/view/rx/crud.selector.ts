import {MemoizedSelector} from "@ngrx/store";

import {Selector} from "./selector";
import {CrudEntity} from "../../rx/model/crud.entity";

export class CrudSelector<E extends CrudEntity> implements Selector<E> {

  name: string;
  value: MemoizedSelector<any, Array<E> | E>;
  payloadType: string;

  type: string;


  constructor(name: string,
              value: MemoizedSelector<any, Array<E> | E>,
              payloadType: string) {

    this.name = name;
    this.value = value;
    this.payloadType = payloadType;

    this.type = `${payloadType}.${name}`;

  }

}
