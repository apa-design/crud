import {MemoizedSelector} from '@ngrx/store';
import {Entity} from '../../rx/model/entity';

export interface Selector<E extends Entity> {
  name: string;
  value: MemoizedSelector<any, Array<E> | E>;
  payloadType: string;

  type: string;
}
