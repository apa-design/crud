import {createFeatureSelector, createSelector, Store} from '@ngrx/store';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {CrudState} from '../../rx/model/crud.state';
import {CrudSelector} from './crud.selector';

import {Selector} from './selector';
import {CrudEntity} from '../../rx/model/crud.entity';

/*
* While to 'be in a context' has the same meaning as 'to have a context',
* context type members need to be injected everywhere where its 'structure' should
* to have an influence on its 'surround', usually we inject a context to a component;)
* */

/*
* As a design pattern is a proxy layer between Store [one data source for the app]
* And application view [gui - generally Angular Components]
* */

/*
 * Basically context object is used to manage 'the one separate' application view
 * Then it can dispatch store [entity | layout] actions to ask store for something
 * And while has wired store's selectors begins to be 'a context' for a view
 * Technically is injected to a view component and there joined to a template via an async pipeline
 * */

export abstract class Context<E extends CrudEntity = CrudEntity>
  extends BehaviorSubject<E> {

  protected selectors: Array<Selector<E>> = new Array<Selector<E>>();

  protected getSelector(feature: string, field: string): CrudSelector<E> {
    return this.selectors.find(value => value.type === `${feature}.${field}`);
  }

  /*
   * setting selector to selectors map
   * */
  protected setSelector(feature: string, field: string): void {
    this.selectors.push(
      new CrudSelector<E>(
        field,
        createSelector(
          createFeatureSelector<CrudState<E>>(feature),
          (state: CrudState<E>) => state['field']
        ),
        feature
      )
    );
  }

  constructor(protected store: Store<any>) {
    super(null);
  }
}
