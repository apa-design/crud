import {BaseFieldDefinition} from './base-field-definition';
import {FieldType} from './field-type.enum';
import {ValidationPattern} from './validation-pattern.enum';

import * as _ from 'lodash';

export class TextFieldDefinition extends BaseFieldDefinition {

  pattern: ValidationPattern;
  minLength: number;
  maxLength: number;


  constructor(name: string, value: string,
              required?: boolean, editable?: boolean, visible?: boolean,
              pattern?: ValidationPattern, minLength?: number, maxLength?: number) {
    super(name, value, FieldType.text, required, editable, visible);

    this.pattern = _.isNil(pattern) ? ValidationPattern.text : pattern;
    this.minLength = _.isNil(minLength) ? null : minLength;
    this.maxLength = _.isNil(maxLength) ? null : maxLength;

  }
}
