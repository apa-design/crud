export enum FieldType {
  text = 'text', range = 'range', select = 'select', calendar = 'calendar'
}
