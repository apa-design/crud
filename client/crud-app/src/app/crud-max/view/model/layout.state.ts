import {FieldDefinition} from "./field-definition";

/*
* Base class for layout states
* */
export class LayoutState {

  fields: Array<FieldDefinition>;

}
