import {FieldDefinition} from './field-definition';
import {FieldType} from './field-type.enum';

export abstract class BaseFieldDefinition implements FieldDefinition {

  name: string;
  value: string;
  type: FieldType;
  required: boolean;
  editable: boolean;
  visible: boolean;


  constructor(name: string, value: string, type: FieldType, required?: boolean, editable?: boolean, visible?: boolean) {
    this.name = name;
    this.value = value;
    this.type = type;
    this.required = required;
    this.editable = editable;
    this.visible = visible;
  }
}
