import {FieldType} from './field-type.enum';

export interface FieldDefinition {

  name: string;
  value: string;
  type: FieldType;
  required: boolean;
  editable: boolean;
  visible: boolean;

}
