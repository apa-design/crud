import {isString} from 'util';

import {Entity} from './entity';
import {EntityStatus} from './entity.status.enum';

export class CrudEntity extends Entity {

  static type: string = 'crud';

  status: number;
  dateCreate: Date;
  dateUpdate: Date;
  userCreate: string;
  userUpdate: string;

  isNew: boolean;

  constructor(id?: number, status?: number,
              dateCreate?: Date, dateUpdate?: Date,
              userCreate?: string, userUpdate?: string) {

    super(id);
    this.status = isNaN(status) ? EntityStatus.none : status;
    this.dateCreate = dateCreate || new Date();
    this.dateUpdate = dateUpdate || new Date();
    this.userCreate = isString(userCreate) ? userCreate : 'user create';
    this.userUpdate = isString(userUpdate) ? userUpdate : 'user create';

    this.isNew = this.id === Number.POSITIVE_INFINITY;

  }
}
