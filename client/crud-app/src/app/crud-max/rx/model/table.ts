import {CrudEntity} from './crud.entity';
import {FieldDefinition} from '../../view/model/field-definition';

export interface Table <T extends CrudEntity  = CrudEntity> {

  entity: T;
  data: Array<T>;
  fields: Array<Array<FieldDefinition>>;

}
