export enum EntityStatus {
  deleted = -2, delete = -1, draft = 0, accept = 1, accepted = 2, none = Number.NEGATIVE_INFINITY
}
