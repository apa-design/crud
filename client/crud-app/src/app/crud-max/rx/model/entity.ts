export abstract class Entity {
  id: number;
  abstract isNew: boolean;
  constructor(id: number) {
    this.id = isNaN(id) ? Number.POSITIVE_INFINITY : id;
  }
}
