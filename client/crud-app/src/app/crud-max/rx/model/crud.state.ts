import {Table} from './table';

import {CrudEntity} from './crud.entity';
import {FieldDefinition} from '../../view/model/field-definition';

export class CrudState<T extends CrudEntity, FD extends FieldDefinition = FieldDefinition> implements Table<T> {

  entity: T;
  data: Array<T>;
  fields: Array<Array<FD>>;

  static getDefaultState<E extends CrudEntity>(): CrudState<E> {
    const state: CrudState<E> = new CrudState<E>();
    state.data = new Array<E>();
    //state.fields = CrudState.fieldsDef;
    state.entity = null;
    return state;
  }

  static getDefaultEntity() {
    const e = new CrudEntity();

    return e;
  }
}
