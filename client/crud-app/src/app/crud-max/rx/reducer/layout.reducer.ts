import {LayoutState} from '../../view/model/layout.state';
import {LayoutAction} from '../action/layout.action';
import {ReducerType} from './reducer.type.enum';

export const layoutReducer =  (defaultState: LayoutState | any) => {
  return (state: LayoutState = defaultState, action: LayoutAction): LayoutState => {
    'use strict';

    const type: string = `${action.payloadType}.${ReducerType.layout}`;
    switch (action.type) {

      /*
      * LAYOUT ACTIONS REDUCERS
      * */
      case type: {
        const item: any = action.payload;
        const type: string = action.payloadType;
        return <LayoutState>{
          ...state
        };

        // TODO - in fail case
      }
      default: {
        return state;
      }
    }
  }
}

