import {CrudState} from '../model/crud.state';
import {CrudEntity} from '../model/crud.entity';
import {CrudAction} from '../action/crud.action';
import {CrudType} from '../model/crud.type.enum';
import {CrudStatus} from '../model/crud.status.enum';

import * as _ from 'lodash';

export const crudReducer =  <E extends CrudEntity = CrudEntity,
  S extends CrudState<E> = CrudState<E>>(payloadType: string, defaultState: S | any) => {

  return (state: S = defaultState, action: CrudAction<E>): S => {
    'use strict';
    switch (action.type) {

      /**
      * Success Actions Reducer
      * */

      /*
       * react on the action (type of success or fail) "create"
       * */
      case `${payloadType}.${CrudType.create}.${CrudStatus.success}`: {
        const item: E = _.isArray(action.payload) ? action.payload[0] : action.payload;
        const newState: S = _.assignIn(state.constructor.apply(state), state);
        newState.data = [...state.data, item];
        newState.entity = null;
        return newState;
      }
      /*
       * react on the action (type of success or fail) "read"
       * */
      case `${payloadType}.${CrudType.read}.${CrudStatus.success}`: {
        const items: Array<E> =  _.isArray(action.payload[0]) ? action.payload[0] : action.payload;
        const newState: S = _.assignIn(state.constructor.apply(state), state);
        newState.data = [...items];
        newState.entity = null;
        return newState;

      }
      /*
       * react on the action (type of success or fail) "update"
       * */
      case `${payloadType}.${CrudType.update}.${CrudStatus.success}`: {
        const item: E = _.isArray(action.payload) ? action.payload[0] : action.payload;
        const items = state.data
          .map(d => (d.id === item.id) ? item : d);
        const newState: S = _.assignIn(state.constructor.apply(state), state);
        newState.data = [...items];
        newState.entity = null;
        return newState;

      }
      /*
       * react on the action (type of success or fail) "dispose"
       * */
      case `${payloadType}.${CrudType.dispose}.${CrudStatus.success}`: {
        const item: E = _.isArray(action.payload) ? action.payload[0] : action.payload;
        const id = item.id;
        const newState: S = _.assignIn(state.constructor.apply(state), state);
        newState.data = state.data.filter(d => d.id !== id);
        newState.entity = null;
        return newState;

      }
      /*
       * react on the action (type of success or fail) "select"
       * */
      case `${payloadType}.${CrudType.select}.${CrudStatus.success}`: {

        /*
        * the new entity is injected with a default data
        * */
        const defaultEntity = state.constructor.prototype.getDefaultEntity();
        const item = action.payload.toString() == 'new' ? _.assignIn(state.constructor.apply(state), defaultEntity) :
          action.payload ? _.assignIn(state.constructor.apply(state), action.payload[0] || action.payload) : null;
        const newState: S = _.assignIn(state.constructor.apply(state), state);
        newState.entity = item;
        return newState;

      }

      default: {
        return state;
      }
    }
  }
}

