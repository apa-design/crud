import {Observable} from 'rxjs/Observable';
import {Inject} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';

import {CrudEffect} from './crud.effect';

import {CrudEntity} from '../model/crud.entity';
import {CrudAction} from '../action/crud.action';
import {StoreActionFactory} from '../action/store-action.factory';
import {CrudService} from '../../service/persistence/crud.service';


/*
* An example of CrudEffect<CrudEntity> implementation
* Can be directly use for simple crud operation using CrudEntity core
* */
export class CrudEntityEffect extends CrudEffect<CrudEntity> {

  protected payloadType: string = CrudEntity.type;

  /*
   * create
   * */
  @Effect()
  createEntity$: Observable<CrudAction<CrudEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readEntity$: Observable<CrudAction<CrudEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateEntity$: Observable<CrudAction<CrudEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeEntity$: Observable<CrudAction<CrudEntity>> = this.dispose();

  constructor(@Inject(Actions) actions$: Actions,
              @Inject(StoreActionFactory) actionFactory: StoreActionFactory,
              @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
  }
}
