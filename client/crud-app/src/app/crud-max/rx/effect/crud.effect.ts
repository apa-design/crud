import {Observable} from 'rxjs/Observable';
import {Actions} from '@ngrx/effects';

import {StoreActionFactory} from '../action/store-action.factory';
import {CrudAction} from '../action/crud.action';
import {CrudEntity} from '../model/crud.entity';
import {CrudType} from '../model/crud.type.enum';
import {CrudStatus} from '../model/crud.status.enum';
import {CrudService} from '../../service/persistence/crud.service';
import {isArray} from 'util';

export abstract class CrudEffect<E  extends CrudEntity> {

  protected abstract payloadType: string;

  /*
   * [create]
   * Reacts on Crud 'create' 'pending' action
   * Invokes service and dispatches 'success' or 'fail' action on response
   * */
  protected create(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType(this.actionFactory.getType(this.payloadType, CrudType.create, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
        let payload = isArray(a.payload) ? a.payload[0] : a.payload;
          return this.service.create(payload, this.payloadType)
            .map((d: E[]) => this.actionFactory.getCrudAction<E>(CrudType.create, CrudStatus.success, this.payloadType, d))
            .catch(() => Observable.of(
              this.actionFactory.getCrudAction<E>(CrudType.create, CrudStatus.fail, this.payloadType, null)
            ));
      });
  }

  /*
   * [read]
   * Reacts on Crud 'read' 'pending' action
   * Invokes service and dispatches 'success' or 'fail' action on response
   * */
  protected read(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.read, CrudStatus.pending))
      .mergeMap((a: CrudAction<E>) => {
        return this.service.getAll(this.payloadType)
          .map((d: E[]) => this.actionFactory.getCrudAction<E>(CrudType.read, CrudStatus.success, this.payloadType, d))
          .catch(() => Observable.of(
            this.actionFactory.getCrudAction<E>(CrudType.read, CrudStatus.fail, this.payloadType, null)
          ));
      });
  }

  /*
   * [update]
   * Reacts on Crud 'update' 'pending' action
   * Invokes service and dispatches 'success' or 'fail' action on response
   * */
  protected update(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.update, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
        let payload = isArray(a.payload) ? a.payload[0] : a.payload;
        return this.service.update(payload, this.payloadType)
          .map((d: E[]) => this.actionFactory.getCrudAction<E>(CrudType.update, CrudStatus.success, this.payloadType, d))
          .catch(() => Observable.of(
            this.actionFactory.getCrudAction<E>(CrudType.update, CrudStatus.fail, this.payloadType, null)
          ));
      });
  }

  /*
   * [dispose]
   * Reacts on Crud 'dispose' 'pending' action
   * Invokes service and dispatches 'success' or 'fail' action on response
   * */
  protected dispose(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.dispose, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
      let payload = isArray(a.payload) ? a.payload[0] : a.payload;
        return this.service.deleteById(payload.id, this.payloadType)
          .map((d) => this.actionFactory.getCrudAction<E>(CrudType.dispose, CrudStatus.success, this.payloadType, d))
          .catch(() => Observable.of(
            this.actionFactory.getCrudAction<E>(CrudType.dispose, CrudStatus.fail, this.payloadType, null)
          ));
      });
  }

  constructor(protected actions$: Actions,
              protected actionFactory: StoreActionFactory,
              protected service: CrudService) {
  }
}
