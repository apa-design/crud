import {Action} from '@ngrx/store';
import {CrudEntity} from '../model/crud.entity';
import {Entity} from '../model/entity';

export abstract class StoreAction<E extends CrudEntity> implements Action {

  type: string;
  storeType: string;
  payloadType: string;
  payload: E | Array<E>;

  constructor(type: string, payload: E | Array<E>) {
    this.payload = payload;
  }
}
