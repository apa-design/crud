import {StoreAction} from './store.action';
import {ReducerType} from '../reducer/reducer.type.enum';

export class LayoutAction extends StoreAction<any> {

  constructor(type: string, payload: any) {
    super(type, payload);
    this.payloadType = type;
    this.storeType = ReducerType.layout;
    this.type = `${this.payloadType}.${this.storeType}`;
  }
}
