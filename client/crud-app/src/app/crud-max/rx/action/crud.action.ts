import {StoreAction} from './store.action';
import {CrudStatus} from '../model/crud.status.enum';
import {CrudEntity} from '../model/crud.entity';

export class CrudAction<E extends CrudEntity = CrudEntity> extends StoreAction<E> {

  /*
  * a new field added to StoreAction<T> api
  * status is used to save/update/delete data
  * from the database through accepting crud operation
  * actually it can be manage by change an entity status
  * */
  status: CrudStatus;

  constructor(type: string, status: CrudStatus, payloadType: string, payload: E | Array<E>) {

    super(type, payload);

    this.storeType = type;
    this.status = status;
    this.payloadType = payloadType;

    this.type = `${this.payloadType}.${this.storeType}.${this.status}`;

  }
}
