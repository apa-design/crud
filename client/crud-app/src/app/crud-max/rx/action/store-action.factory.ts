import {Injectable} from '@angular/core';

import {CrudAction} from './crud.action';
import {CrudType} from '../model/crud.type.enum';
import {CrudStatus} from '../model/crud.status.enum';
import {LayoutAction} from './layout.action';
import {CrudEntity} from '../model/crud.entity';

Injectable();
export class StoreActionFactory {

  getType(payloadType: string, type: string, status?: string): string {
    return status !== null ? `${payloadType}.${type}.${status}` : `${payloadType}.${type}`;
  }

  getCrudAction<T extends CrudEntity = CrudEntity>
  (type: CrudType, status: CrudStatus, payloadType: string, payload: any = null): CrudAction<T> {
    return new CrudAction<T>(type, status, payloadType, payload);
  }

  getLayoutAction(type: string, payload: any = null): LayoutAction {
    return new LayoutAction(type, payload);
  }

  getAction<T extends CrudEntity = CrudEntity>(...args):
    CrudAction<T> | LayoutAction {

    switch (args.length) {
      case 4 :
      case 3 :
        return this.getCrudAction.apply(this, args);
      case 2 :
      case 1 :
        return this.getLayoutAction.apply(this, args);
    };

    return null;
  }

  constructor() {
  }
}
