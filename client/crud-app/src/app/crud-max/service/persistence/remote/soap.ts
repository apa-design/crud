import {Observable} from 'rxjs/Observable';

import {CrudPersistence} from '../crud-persistence';

export class Soap<T> implements CrudPersistence<T> {

  constructor(private soapClient: any) {

  }
  set location(path: string) {
  }
  get location(): string {
    throw new Error('method is not implemented');
  }
  public getAll(): Observable<Array<T>> {
    throw new Error('method is not implemented');
  }
  getById(id: string): Observable<T> {
    throw new Error('method is not implemented');
  }
  public getRange(start: number, amount: number): Observable<Array<T>> {
    throw new Error('method is not implemented');
  }
  public create(item: T): Observable<T> {
    throw new Error('method is not implemented');
  }
  public update(item: T): Observable<T> {
    throw new Error('method is not implemented');
  }
  public deleteById(id: string): Observable<T> {
    throw new Error('method is not implemented');
  }
  count(): Observable<number> {
    throw new Error('method is not implemented');
  }
}
