import {HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CrudConfig} from './crud-config';
import {StoreActionFactory} from './rx/action/store-action.factory';
import {CrudService} from './service/persistence/crud.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    StoreActionFactory,
    CrudConfig,
    CrudService
  ],
  exports: [
  ]
})
export class CrudMaxModule { }
