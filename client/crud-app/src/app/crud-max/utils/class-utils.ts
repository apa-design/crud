import * as _ from 'lodash';
export class ClassUtils {

  static getInstanceCopy(instance: any) : Function {
    return _.assignIn(instance.__proto__.constructor.apply(instance), instance);
  }


  static getInstanceClass(instance: any) : Function {
      return instance.__proto__.constructor;
    }

}
