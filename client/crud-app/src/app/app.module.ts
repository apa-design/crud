import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import {AppComponent} from './app.component';
import {CrudMaxModule} from './crud-max/crud-max.module';
import {StoreActionFactory} from './crud-max/rx/action/store-action.factory';
import {CrudConfig} from './crud-max/crud-config';
import {CrudService} from './crud-max/service/persistence/crud.service';
import {reducerToken, reducerProvider} from './rx/application.reducer';
import {DictionaryEntityEffect} from './rx/dictionary/dictionary.effect';
import {CustomerEntityEffect} from './rx/customer/customer.effect';


/*
* The way to create, declare and manage reducers is depended on proper application compiling
* TODO: find a possibility to prepare and manage reducer as a normal TypeScript class
* */

/*
* This is a place to declare all global reducers [actually the stores] collected in ./application.reducer.ts
* also a place to declare global effects - actually effects are composed for entities states only
* A reducer should be disposed asap after the application will stop to need its services
* */

/*
* Also imports the rx-crud library module
* */
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CrudMaxModule,
    BrowserModule,
    StoreModule.forRoot(reducerToken),
    EffectsModule.forRoot([CustomerEntityEffect,
      DictionaryEntityEffect]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    reducerProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

