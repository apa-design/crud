import { Component } from '@angular/core';
import {Store} from '@ngrx/store';

import {StoreActionFactory} from './crud-max/rx/action/store-action.factory';
import {CrudType} from './crud-max/rx/model/crud.type.enum';
import {CrudStatus} from './crud-max/rx/model/crud.status.enum';
import {DictionaryEntity} from './rx/dictionary/dictionary.entity';
import {CustomerEntity} from "./rx/customer/customer.entity";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'test application';


  loadAll() {
    this.loadDictionary();
    this.loadCustomer();
  }
  loadDictionary() {
    this.store.dispatch(this.factory.getCrudAction(CrudType.read, CrudStatus.pending, DictionaryEntity.type));
  }

  loadCustomer() {
    this.store.dispatch(this.factory.getCrudAction(CrudType.read, CrudStatus.pending, CustomerEntity.type));
  }
  constructor(private factory: StoreActionFactory, private store: Store<any>) {

  }
}
