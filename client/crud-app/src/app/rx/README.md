## rx application package

A package containing application model and features to manage a view
model. Generally there should be defined typed effect, extending the base
'crud effect', sometimes we will need some new reducers or maybe actions,
then it is also an actual package to place them.

### Structure

The structure of the package is as flat as possible. So on the root we have a reducers composition to use in our application, and next - needed feture packages, flat inside.

At least the one exported composed reducers, placed in the file usually named 'application.reducers.ts' and finally exported as {reducerProvider, reducerToken}.
More files with reducer definition should be joined and also exported as mentioned above, and also placed on the root of rx package.
Finally, on the top we should place the file with redux operators imports. The file should be imported to main.ts file. [rxjs-operators.ts]

	-- rx
			*rxjs-operators.ts
			*applicatio.reducer.ts
			-- feature1
						*feature1.entity.ts
						*feature1.state.ts
						*feature1.effect.ts
						feature1.reducer.ts
			-- feature2
    				*feature2.entity.ts
    				*feature2.state.ts
    				*feature2.effect.ts
            // no reducer - is not required and usually we won't define any
						

### Elements

// TODO

### No NgModule

While in the module any injectable services are not defined, we don't need to create NgModule, so don't do it!
The classes take place in the application throught the 'redux operations'. We don't need more angular's dependencies here...
