import {CrudEntity} from '../../crud-max/rx/model/crud.entity';

export class CustomerEntity extends CrudEntity {

  static type: string = 'customer';

  cif: string;
  classificationBzwbk: string;
  classificationNbp: string;
  classificationNbpRpw: string;
  customerType: string;
  dateOfBirth: Date;
  firstName: string;
  identityDocument: string;
  language: string;
  lastName: string;
  name: string;
  nip: string;
  numberBm2: string;
  pesel: string;
  regon: string;
  taxOffice: string;


  constructor(id?: number, status?: number, dateCreate?: Date,
              dateUpdate?: Date, userCreate?: string, userUpdate?: string,
              cif?: string, classificationBzwbk?: string, classificationNbp?: string, classificationNbpRpw?: string,
              customerType?: string, dateOfBirth?: Date, firstName?: string, identityDocument?: string, language?: string,
              lastName?: string, name?: string, nip?: string, numberBm2?: string, pesel?: string, regon?: string,
              taxOffice?: string) {

    super(id, status, dateCreate, dateUpdate, userCreate, userUpdate);
    this.cif = cif;
    this.classificationBzwbk = classificationBzwbk;
    this.classificationNbp = classificationNbp;
    this.classificationNbpRpw = classificationNbpRpw;
    this.customerType = customerType;
    this.dateOfBirth = dateOfBirth;
    this.firstName = firstName;
    this.identityDocument = identityDocument;
    this.language = language;
    this.lastName = lastName;
    this.name = name;
    this.nip = nip;
    this.numberBm2 = numberBm2;
    this.pesel = pesel;
    this.regon = regon;
    this.taxOffice = taxOffice;

  }
}
