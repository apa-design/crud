import {CrudState} from '../../crud-max/rx/model/crud.state';

import {FieldDefinition} from '../../crud-max/view/model/field-definition';
import {CustomerEntity} from './customer.entity';
import {CrudEntity} from '../../crud-max/rx/model/crud.entity';

export class CustomerState<E extends CrudEntity = CustomerEntity> extends CrudState<E> {

  static fieldsDef: FieldDefinition[][] = [[]];

  fields: FieldDefinition[][] = CustomerState.fieldsDef;

  static getDefaultState<CE extends CrudEntity = CustomerEntity>(): CustomerState<CE> {
    const state: CustomerState<CE> = new CustomerState<CE>();
    state.data = new Array<CE>();
    state.fields = CustomerState.fieldsDef;
    state.entity = null;
    return state;
  }

  static getDefaultEntity(): CustomerEntity {
    const e: CustomerEntity = new CustomerEntity(Number.POSITIVE_INFINITY, null, null,
      null, null,
      null, 'cif', 'classification bzwbk', 'classification nbp', 'classification nbp rpw',
      'customer type', null, 'first name');
    return e;
  }
}
