import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {Inject, Injectable} from '@angular/core';

import {CrudEffect} from '../../crud-max/rx/effect/crud.effect';

import {CrudAction} from '../../crud-max/rx/action/crud.action';
import {StoreActionFactory} from '../../crud-max/rx/action/store-action.factory';
import {CrudService} from '../../crud-max/service/persistence/crud.service';
import {CustomerEntity} from './customer.entity';

export class CustomerEntityEffect extends CrudEffect<CustomerEntity> {

  protected payloadType: string = CustomerEntity.type;

  /*
   * create
   * */
  @Effect()
  createDictionary$: Observable<CrudAction<CustomerEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readDictionary$: Observable<CrudAction<CustomerEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateDictionary$: Observable<CrudAction<CustomerEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeDictionary$: Observable<CrudAction<CustomerEntity>> = this.dispose();

  constructor(@Inject(Actions) actions$: Actions,
              @Inject(StoreActionFactory) actionFactory: StoreActionFactory,
              @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
  }
}
