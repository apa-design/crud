import {InjectionToken} from "@angular/core";
import {ActionReducer, ActionReducerMap, combineReducers} from '@ngrx/store';

import {crudReducer} from '../crud-max/rx/reducer/crud.reducer';
import {CustomerState} from './customer/customer.state';
import {DictionaryState} from './dictionary/dictionary.state';
import {DictionaryEntity} from './dictionary/dictionary.entity';
import {CustomerEntity} from './customer/customer.entity';
import {layoutReducer} from '../crud-max/rx/reducer/layout.reducer';
import {LayoutState} from "../crud-max/view/model/layout.state";



export interface ApplicationState {
  customer: CustomerState,
  dictionary: DictionaryState,
  layout: LayoutState
}

const reducersDef: ActionReducerMap<ApplicationState> = {
  customer: crudReducer<CustomerEntity, CustomerState>(CustomerEntity.type, CustomerState.getDefaultState()),
  dictionary: crudReducer<DictionaryEntity, DictionaryState>(DictionaryEntity.type, DictionaryState.getDefaultState()),
  layout: layoutReducer(new LayoutState())
};

export const reducers: ActionReducer<ApplicationState> = combineReducers(reducersDef);

export const reducerToken = new InjectionToken<ActionReducerMap<ApplicationState>>('Reducers');

export function getReducers() {
  return {
    app: reducers,
  };
}

export const reducerProvider = [
  { provide: reducerToken, useFactory: getReducers }
];
