import {CrudState} from '../../crud-max/rx/model/crud.state';

import {DictionaryEntity} from './dictionary.entity';
import {FieldDefinition} from '../../crud-max/view/model/field-definition';
import {CrudEntity} from '../../crud-max/rx/model/crud.entity';

export class DictionaryState<E extends CrudEntity = DictionaryEntity> extends CrudState<E> {

  static fieldsDef: FieldDefinition[][] = [[]];

  fields: FieldDefinition[][] = DictionaryState.fieldsDef;


  static getDefaultState<DE extends CrudEntity = DictionaryEntity>(): DictionaryState<DE> {
    const state: DictionaryState<DE> = new DictionaryState<DE>();
    state.data = new Array<DE>();
    state.fields = DictionaryState.fieldsDef;
    state.entity = null;
    return state;
  }

  static getDefaultEntity(): DictionaryEntity {
    const e: DictionaryEntity = new DictionaryEntity(Number.POSITIVE_INFINITY, null, null,
      null, null,
      null, 'field name', 'table name', 'value 1', 'value 2',
      'market', 'deposit', 'description');
    return e;
  }
}
