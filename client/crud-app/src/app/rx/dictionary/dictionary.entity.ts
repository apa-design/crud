import {CrudEntity} from '../../crud-max/rx/model/crud.entity';
import {EntityStatus} from '../../crud-max/rx/model/entity.status.enum';

export class DictionaryEntity extends CrudEntity {

  static type: string = 'dictionary';

  fieldName: string;
  tableName: string;
  value1: string;
  value2: string;
  market: string;
  deposit: string;
  description: string;

  constructor (id?: number, status?: EntityStatus,
               dateCreate?: Date, dateUpdate?: Date, userCreate?: string, userUpdate?: string,
               fieldName?: string,
               tableName?: string,
               value1?: string, value2?: string,
               market?: string, deposit?: string, description?: string) {

    super(id, status, dateCreate, dateUpdate, userCreate, userUpdate);

    this.fieldName = fieldName;
    this.tableName = tableName;
    this.value1 = value1;
    this.value2 = value2;
    this.market = market;
    this.deposit = deposit;
    this.description = description;
  }
}
