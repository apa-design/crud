import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {Inject, Injectable} from '@angular/core';

import {CrudEffect} from '../../crud-max/rx/effect/crud.effect';

import {DictionaryEntity} from './dictionary.entity';
import {CrudAction} from '../../crud-max/rx/action/crud.action';
import {StoreActionFactory} from '../../crud-max/rx/action/store-action.factory';
import {CrudService} from '../../crud-max/service/persistence/crud.service';

export class DictionaryEntityEffect extends CrudEffect<DictionaryEntity> {

  protected payloadType: string = DictionaryEntity.type;

  /*
   * create
   * */
  @Effect()
  createDictionary$: Observable<CrudAction<DictionaryEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readDictionary$: Observable<CrudAction<DictionaryEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateDictionary$: Observable<CrudAction<DictionaryEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeDictionary$: Observable<CrudAction<DictionaryEntity>> = this.dispose();

  constructor(@Inject(Actions) actions$: Actions,
              @Inject(StoreActionFactory) actionFactory: StoreActionFactory,
              @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
  }
}
