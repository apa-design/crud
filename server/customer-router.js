'use strict';

const router = require('./router');
const uuid = require('node-uuid');
const low = require('lowdb');
const path = require('path');
const storage = require('lowdb/adapters/FileSync');

const adapter = new storage(path.join(__dirname, './storage/customer.json'));
const db = low(adapter);

router.post('/customer', (req, res) => {
  let data = req.body;
  data.id = uuid.v4();
  let b = db.get('customer').push(data).last().value();
  res.status(200).json(b);
});

router.get('/customer', (req, res) => {
  res.status(200).json(db.get('customer').value());
});

router.get('/customer/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('customer').find({id}).value();
  res.status(200).json(b);
});

router.put('/customer/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('customer').find({id}).assign(req.body).value();
  res.status(200).json(b);
});

router.delete('/customer/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('customer').find({id}).value();
  db.get('customer').remove({id}).value();
  res.status(200).json(b);
});

module.exports = router;
